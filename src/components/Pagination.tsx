import React from 'react';
import { Link } from 'react-router-dom';

type Props = {
  total: number;
  name: any;
  postsPerPage: any;
};

const Pagination = ({ total, name, postsPerPage }: Props): JSX.Element => {
  const pageNumbers: Array<number> = [];
  for (let i = 1; i <= Math.ceil(total / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div>
      <ul className="pagination">
        {pageNumbers.map((number) => (
          <li key={number} className="pageItem">
            <Link to={`?name=${name}&page=${number}`} className="page-link">
              {number}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Pagination;
