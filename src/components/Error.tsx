import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const Error = () => {
  return (
    <div className="container">
       <div className="not-found">
      <h2>Sorry</h2>
      <p>That page cannot be found</p>
      <Link to="/">Back to the homepage</Link>
    </div>
    </div>
   
  );
};

export default Error;
