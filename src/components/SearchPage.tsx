import '../App.css';
import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import axios from 'axios';
import { searchHeroes } from '../api';
import Characters from './Characters';
import SearchBar from './SearchBar';
import Pagination from '@atlaskit/pagination';
import Spinner from '@atlaskit/spinner';
import PageButton from './PageButton';

interface MyState {
  valueInput: string;
  heroesArray: Character[];
  postsPerPage: number;
  total: number;
  sort: string;
  loading: boolean;
}

type Character = {
  id: number;
  thumbnail: Thumbnail;
  name: string;
  description: string;
};

type Thumbnail = {
  path: string;
  extension: string;
};

class SearchPage extends Component<RouteComponentProps, MyState> {
  constructor(props: RouteComponentProps) {
    super(props);

    this.state = {
      valueInput: '',
      heroesArray: [],
      postsPerPage: 3,
      total: 0,
      sort: 'modified',
      loading: false,
    };
  }

  // получить query-параметры
  private getNameQuery = () => {
    return new URLSearchParams(this.props.location.search).get('name');
  };
  private getOffsetQuery = () => {
    return Number(new URLSearchParams(this.props.location.search).get('offset'));
  };
  private getPageQuery = () => {
    return Number(new URLSearchParams(this.props.location.search).get('page'));
  };

  // следить за изменением в input
  private handlerUpdateSearch = (el: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({
      valueInput: el.target.value,
    });
  };

  //обработчик клика по кнопке поиска
  private searching = (e: React.SyntheticEvent<EventTarget>): void => {
    e.preventDefault;
    const { valueInput } = this.state;
    if (valueInput) {
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.history.push(`/?name=${valueInput}`);
        },
      );
    }
  };

  public componentDidMount = (): void => {
    const page = this.getPageQuery();
    const name = this.getNameQuery();
    const { sort } = this.state;
    if (name !== null && name !== '') {
      this.setState(
        {
          loading: true,
          valueInput: name,
        },
        () => this.getHeroes(this.state.valueInput, page, sort),
      );
    }
  };

  //запрос
  private getHeroes = (name: string | null, page: number, sort: string) => {
    const limit = 3;
    const offset = page === 0 ? page * limit : (page - 1) * limit;
    setTimeout(() => {
      searchHeroes(name, limit, offset, sort).then(
        (data) => {
          const heroes: Character[] = data.data.results;
          if(heroes.length !== 0) {
            this.setState({
              heroesArray: heroes,
              total: data.data.total,
              loading: false,
            })
          } else {
            this.props.history.push(`/error`);
          }
        }
      );
    }, 3000);
  };

  public componentDidUpdate = (prevProps: RouteComponentProps): void => {
    const name = this.getNameQuery();
    const page = this.getPageQuery();
    if (prevProps.location.key !== this.props.location.key && name) {
      this.getHeroes(name, page, this.state.sort);
    }
  };

  private sort = (): void => {
    const name = this.getNameQuery();
    const page = this.getPageQuery();
    this.setState(
      (state) => {
        return {
          sort: state.sort === 'modified' ? '-modified' : 'modified',
        };
      },
      () => this.getHeroes(name, page, this.state.sort),
    );
  };

  private pagination = (page: number): void => {
    const name = this.getNameQuery();
    const url = page === 1 ? `/?name=${name}` : `/?name=${name}&page=${page}`;
    this.props.history.push(url);
  };

  render() {
    const page = this.getPageQuery();
    const { valueInput, heroesArray, total, loading } = this.state;
    const pages = new Array(Math.ceil(total / 3)).fill(1).map((a, i) => i + 1);
    return (
      <div className="container">
        {loading && (
          <div className="spinner">
            <Spinner size="large" appearance="invert" />
          </div>
        )}
        <SearchBar
          value={valueInput}
          onChange={this.handlerUpdateSearch}
          onClick={this.searching}
          changeOrder={this.sort}
        />
        {!loading && <Characters heroesArray={heroesArray} />}

        {heroesArray.length !== 0 && (
          <div className="pagination">
            <Pagination
              components={{
                Page: (props) => {
                  return <PageButton {...this.props} pageNum={props.page} pagination={this.pagination} />;
                },
              }}
              pages={pages}
              onChange={(e: React.SyntheticEvent<Event>, page: number) => {
                this.pagination(page);
              }}
              selectedIndex={page === 0 ? page : page - 1}
            />
          </div>
        )}
      </div>
    );
  }
}

export default SearchPage;
