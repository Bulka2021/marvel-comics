import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Comics from './components/Comics';
import Header from './components/Header';
import Error from './components/Error';
import SearchPage from './components/SearchPage';

class App extends Component {
  render(): JSX.Element {
    return (
      <Router>
        <div className="App">
          <Header />
          <Switch>
            <Route path="/" exact component={SearchPage} />
            <Route path="/comics/:id" component={Comics} />
            <Route path="*" strict component={Error} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
