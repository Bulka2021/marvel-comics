import React from 'react';
import { RouteComponentProps } from 'react-router';
import Button from '@atlaskit/button';

type PageButtonProps = {
  pageNum: number;
  pagination: (pageNum: number) => void;
};

const PageButton = (props: RouteComponentProps & PageButtonProps): JSX.Element => {
  const page = new URLSearchParams(props.location.search).get('page');
  const checking = page === null ? 1 : page;
  const property = props.pageNum === Number(checking) ? true : false;

  return (
    <div className={property ? 'active_page_button' : ''}>
      <Button onClick={() => props.pagination(props.pageNum)}>{props.pageNum}</Button>
    </div>
  );
};

export default PageButton;
