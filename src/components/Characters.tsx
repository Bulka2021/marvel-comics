import React from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import Avatar, { AvatarItem } from '@atlaskit/avatar';
import Button from '@atlaskit/button';

type Props = {
  heroesArray: Hero[];
};

type Hero = {
  id: number;
  thumbnail: Thumbnail;
  name: string;
  description: string;
};

type Thumbnail = {
  path: string;
  extension: string;
};

const Characters = ({ heroesArray }: Props): JSX.Element => {
  return (
    <div className="heroesList">
      {heroesArray.map((el) => (
        <div key={el.id} className="heroesList_item">
          <div className="heroItem">
            <div className="heroes_info">
              <AvatarItem
                backgroundColor="#393939"
                avatar={<Avatar appearance="square" src={`${el.thumbnail.path}/standard_large.jpg`} size="xlarge" />}
                primaryText={el.name}
              />
              <div className="seemore_btn">
                <Link to={`/comics/${el.id}`}>
                  <Button appearance="primary">See more</Button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Characters;
