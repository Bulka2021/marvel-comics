import React from 'react';
import Textfield from '@atlaskit/textfield';
import Button from '@atlaskit/button';

type Props = {
  value: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
  changeOrder: React.MouseEventHandler<HTMLButtonElement>;
};

const SearchBar = ({ value, onChange, onClick, changeOrder }: Props) => {
  return (
    <div className="searching">
      <Textfield
        name="basic"
        aria-label="default text field"
        onChange={onChange}
        type="text"
        value={value}
        placeholder="Search"
      />
      <Button className="sort_btn" onClick={changeOrder} appearance="primary">
        Sort
      </Button>
      <Button className="search_btn" onClick={onClick} appearance="primary">
        Search!
      </Button>
    </div>
  );
};

export default SearchBar;
