import React, { Component } from 'react';
import axios from 'axios';
import { RouteComponentProps } from 'react-router';
import { v1 as uuidv1 } from 'uuid';
import { getComics } from '../api';
import Avatar, { AvatarItem } from '@atlaskit/avatar';

interface MyState {
  comics: ComicsList[];
}

type ComicsList = {
  id: number;
  title: number;
  thumbnail: Thumbnail;
  pageCount: number;
};

type Thumbnail = {
  path: string;
};

type ParamsObj = {
  id: string;
};

class Comics extends Component<RouteComponentProps<ParamsObj>, MyState> {
  state: Readonly<MyState> = {
    comics: [],
  };

  public componentDidMount(): void {
    const { match } = this.props;
    this.axiosComics(Number(match.params.id));
  }

  private axiosComics = (id: number | null): void => {
    getComics(id).then((res) => {
      const comics: ComicsList[] = res.data.results;
      this.setState({
        comics: comics,
      });
    });
  };

  private comicsList = () => {
    const { comics } = this.state;
    const list = comics.map((el) => {
      return (
        <React.Fragment key={el.id}>
          <div className="comicItem">
            <div className="comic_info">
              <AvatarItem
                backgroundColor="#393939"
                avatar={
                  <Avatar appearance="square" src={`${el.thumbnail.path}/standard_fantastic.jpg`} size="xlarge" />
                }
                primaryText={el.title}
                secondaryText={`Pages: ${el.pageCount}`}
              />
            </div>
          </div>
        </React.Fragment>
      );
    });
    return (
      <div className="container">
        <div>
          <h1 className="comics_hd">Comics List</h1>
          <div className="comicsList">{list}</div>
          {comics.length === 0 && <div className="noResults">there are no comics with such hero</div>}
        </div>
      </div>
    );
  };

  public render(): JSX.Element {
    return <>{this.comicsList()}</>;
  }
}

export default Comics;
