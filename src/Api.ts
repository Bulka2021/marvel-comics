import axios from 'axios';

type ResponseDataHeroes = {
  data: HeroesData;
};

type HeroesData = {
  results: Character[];
  offset: number;
  total: number;
};

type Character = {
  id: number;
  thumbnail: Thumbnail;
  name: string;
  description: string;
};

type Thumbnail = {
  path: string;
  extension: string;
};

type ResponseDataComics = {
  data: ComicsData;
};

type ComicsData = {
  results: ComicsList[];
};

type ComicsList = {
  id: number;
  title: number;
  pageCount: number;
  thumbnail: Thumbnail;
  description: string;
  prices: Prices[];
  digitalId: number;
};

type Prices = {
  type: string;
  price: number;
};

export const searchHeroes = async (
  nameStartsWith: string | null,
  limit: number,
  offset: number,
  orderBy?: string,
): Promise<ResponseDataHeroes> => {
  try {
    const response = await axios.get<ResponseDataHeroes>('https://gateway.marvel.com:443/v1/public/characters', {
      params: {
        nameStartsWith: nameStartsWith,
        limit: limit,
        offset: offset,
        orderBy: orderBy,
        apikey: 'e301b0fee4c42f22636419298243a633',
      },
    });
    return response.data;
  } catch (err) {
    console.error(err);
    throw new Error('Error');
  }
};

export const getComics = async (id: number | null): Promise<ResponseDataComics> => {
  try {
    const response = await axios.get<ResponseDataComics>(
      `https://gateway.marvel.com:443/v1/public/characters/${id}/comics`,
      {
        params: {
          apikey: 'e301b0fee4c42f22636419298243a633',
        },
      },
    );
    return response.data;
  } catch (err) {
    console.error(err);
    throw new Error('Error');
  }
};
